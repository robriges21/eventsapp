"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventsController = void 0;

var _index = _interopRequireDefault(require("@babel/runtime/regenerator/index.js"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck.js"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass.js"));

var _event = _interopRequireDefault(require("../dataProvider/event"));

var EventsController =
/*#__PURE__*/
function () {
  function EventsController() {
    (0, _classCallCheck2["default"])(this, EventsController);
  }

  (0, _createClass2["default"])(EventsController, [{
    key: "getAllEvents",
    value: function getAllEvents(request, response) {
      var result;
      return _index["default"].async(function getAllEvents$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _index["default"].awrap(_event["default"].getAll());

            case 2:
              result = _context.sent;
              result ? response.jsonp(result) : response.sendStatus(500);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      });
    }
  }, {
    key: "addEvent",
    value: function addEvent(request, response) {
      var event, result;
      return _index["default"].async(function addEvent$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              event = request.body;
              _context2.next = 3;
              return _index["default"].awrap(_event["default"].add(event));

            case 3:
              result = _context2.sent;
              result ? response.sendStatus(500) : response.sendStatus(200);

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      });
    }
  }, {
    key: "getEventInfo",
    value: function getEventInfo(request, response) {
      var id, result;
      return _index["default"].async(function getEventInfo$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              id = request.params.id;
              _context3.next = 3;
              return _index["default"].awrap(_event["default"].getById(id));

            case 3:
              result = _context3.sent;
              result ? response.jsonp(result) : response.sendStatus(500);

            case 5:
            case "end":
              return _context3.stop();
          }
        }
      });
    }
  }, {
    key: "addParticipant",
    value: function addParticipant(request, response) {
      var id, participant, result;
      return _index["default"].async(function addParticipant$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              id = request.params.id;
              participant = request.body;
              _context4.next = 4;
              return _index["default"].awrap(_event["default"].addParticipant(id, participant));

            case 4:
              result = _context4.sent;
              result ? response.jsonp(result) : response.sendStatus(500);

            case 6:
            case "end":
              return _context4.stop();
          }
        }
      });
    }
  }, {
    key: "removeParticipant",
    value: function removeParticipant(request, response) {
      return _index["default"].async(function removeParticipant$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
            case "end":
              return _context5.stop();
          }
        }
      });
    }
  }]);
  return EventsController;
}();

exports.EventsController = EventsController;