"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoginController = void 0;

var _index = _interopRequireDefault(require("@babel/runtime/regenerator/index.js"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck.js"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass.js"));

var _user = _interopRequireDefault(require("../dataProvider/user"));

var LoginController =
/*#__PURE__*/
function () {
  function LoginController() {
    (0, _classCallCheck2["default"])(this, LoginController);
  }

  (0, _createClass2["default"])(LoginController, [{
    key: "doLogin",
    value: function doLogin(request, response) {
      return _index["default"].async(function doLogin$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              response.send({
                token: request.user
              });

            case 1:
            case "end":
              return _context.stop();
          }
        }
      });
    }
  }, {
    key: "register",
    value: function register(request, response) {
      var user, result;
      return _index["default"].async(function register$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              user = request.body;
              _context2.next = 3;
              return _index["default"].awrap(_user["default"].add(user));

            case 3:
              result = _context2.sent;
              result ? response.sendStatus(500) : response.sendStatus(200);

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      });
    }
  }]);
  return LoginController;
}();

exports.LoginController = LoginController;