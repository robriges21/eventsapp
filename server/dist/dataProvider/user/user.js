"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var Schema = _mongoose.Schema;
var userSchema = new Schema({
  name: {
    type: String,
    require: [true, 'Name field is required']
  },
  username: {
    type: String,
    required: [true, 'User Name field is required']
  },
  email: {
    type: String,
    required: [true, 'Date field is required']
  },
  password: {
    type: String,
    required: [true, 'Password field is required']
  }
}, {
  collection: 'user2'
});

var _default = (0, _mongoose.model)('User', userSchema);

exports["default"] = _default;