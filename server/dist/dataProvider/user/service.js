"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function userService(userModel) {
  var service = {
    create: create,
    findOne: findOne,
    findOneAndUpdate: findOneAndUpdate
  };

  function create(values) {
    var user = userModel(values);
    return save(user);
  }

  function findOne(username) {
    return new Promise(function (resolve, reject) {
      userModel.findOne({
        "username": username
      }, function (err, user) {
        if (err) reject(err);
        resolve(user);
      });
    });
  }

  function findOneAndUpdate() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return new Promise(function (resolve, reject) {
      userModel.findOne({
        "email": obj.email
      }, function (err, user) {
        if (err) reject(err);
        user.save().then(function () {
          return resolve(user);
        })["catch"](function (err) {
          return resolve(err);
        });
      });
    });
  }

  function save(user) {
    return new Promise(function (resolve, reject) {
      user.save(function (err) {
        if (err) reject(err);
        resolve('User created!');
      });
    });
  }

  return service;
}

var _default = userService;
exports["default"] = _default;