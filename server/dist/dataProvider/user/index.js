"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _index = _interopRequireDefault(require("@babel/runtime/regenerator/index.js"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck.js"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass.js"));

var _user = _interopRequireDefault(require("./user"));

var _service = _interopRequireDefault(require("./service"));

var dataProvider = (0, _service["default"])(_user["default"]);

var UserProvider =
/*#__PURE__*/
function () {
  function UserProvider(dataProvider) {
    (0, _classCallCheck2["default"])(this, UserProvider);
    this.provider = dataProvider;
  }

  (0, _createClass2["default"])(UserProvider, [{
    key: "add",
    value: function add(user) {
      return _index["default"].async(function add$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              this.provider.create(user).then(function (res) {
                return res;
              })["catch"](function (err) {
                return err;
              });

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "get",
    value: function get(email) {
      return _index["default"].async(function get$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt("return", this.provider.findOne(email).then(function (result) {
                return result;
              })["catch"](function (err) {
                return err;
              }));

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, null, this);
    }
  }]);
  return UserProvider;
}();

var _default = new UserProvider(dataProvider);

exports["default"] = _default;