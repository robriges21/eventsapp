"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function eventService(eventModel) {
  var service = {
    create: create,
    findAll: findAll,
    findOne: findOne,
    remove: remove,
    findOneAndUpdate: findOneAndUpdate
  };

  function create(values) {
    var event = eventModel(values);
    return save(event);
  }

  function findAll() {
    return new Promise(function (resolve, reject) {
      eventModel.find({}, function (err, events) {
        if (err) reject(err);
        resolve(events);
      });
    });
  }

  function findOne(eventId) {
    return new Promise(function (resolve, reject) {
      eventModel.findOne({
        "_id": eventId
      }, function (err, events) {
        if (err) reject(err);
        resolve(events);
      });
    });
  }

  function findOneAndUpdate() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return new Promise(function (resolve, reject) {
      eventModel.findOne({
        "_id": obj.id
      }, function (err, event) {
        if (err) reject(err);
        event.subscribed.push(obj.participant);
        event.save().then(function () {
          return resolve(event);
        })["catch"](function (err) {
          return resolve(err);
        });
      });
    });
  }

  function remove(options) {
    return new Promise(function (resolve, reject) {
      eventModel.findOneAndRemove(options, function (err) {
        if (err) reject(err);
        resolve();
      });
    });
  }

  function save(event) {
    return new Promise(function (resolve, reject) {
      event.save(function (err) {
        if (err) reject(err);
        resolve('Event created!');
      });
    });
  }

  return service;
}

var _default = eventService;
exports["default"] = _default;