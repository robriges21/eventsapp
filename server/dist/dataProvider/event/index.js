"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _index = _interopRequireDefault(require("@babel/runtime/regenerator/index.js"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck.js"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass.js"));

var _event = _interopRequireDefault(require("./event"));

var _service = _interopRequireDefault(require("./service"));

var dataProvider = (0, _service["default"])(_event["default"]);

var EventProvider =
/*#__PURE__*/
function () {
  function EventProvider(dataProvider) {
    (0, _classCallCheck2["default"])(this, EventProvider);
    this.provider = dataProvider;
  }

  (0, _createClass2["default"])(EventProvider, [{
    key: "add",
    value: function add(event) {
      return _index["default"].async(function add$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              this.provider.create(event).then(function () {
                return true;
              })["catch"](function () {
                return false;
              });

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "getAll",
    value: function getAll() {
      return _index["default"].async(function getAll$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt("return", this.provider.findAll().then(function (result) {
                return result;
              })["catch"](function () {
                return null;
              }));

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "getById",
    value: function getById(id) {
      return _index["default"].async(function getById$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt("return", this.provider.findOne(id).then(function (result) {
                return result;
              })["catch"](function () {
                return null;
              }));

            case 2:
            case "end":
              return _context3.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "addParticipant",
    value: function addParticipant(id, participant) {
      return _index["default"].async(function addParticipant$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt("return", this.provider.findOneAndUpdate({
                id: id,
                participant: participant
              }).then(function (result) {
                return result;
              })["catch"](function () {
                return null;
              }));

            case 2:
            case "end":
              return _context4.stop();
          }
        }
      }, null, this);
    }
  }]);
  return EventProvider;
}();

var _default = new EventProvider(dataProvider);

exports["default"] = _default;