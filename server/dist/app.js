"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _morgan = _interopRequireDefault(require("morgan"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _cors = _interopRequireDefault(require("cors"));

var _events = _interopRequireDefault(require("./routes/events"));

var _login = _interopRequireDefault(require("./routes/login"));

var app = (0, _express["default"])();
app.use((0, _morgan["default"])('dev'));
app.use(_express["default"].json());
app.use(_express["default"].urlencoded({
  extended: false
}));
app.use((0, _cookieParser["default"])());
app.use((0, _cors["default"])());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT ,DELETE');
  next();
});
app.use('/events', _events["default"]);
app.use('/login', _login["default"]);
var config = {
  db: 'mongodb://localhost/events_app'
};

_mongoose["default"].connect(config.db, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var _default = app;
exports["default"] = _default;