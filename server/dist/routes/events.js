"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _eventsController = require("../controllers/eventsController");

var _passport = _interopRequireDefault(require("passport"));

var _passportHttpBearer = _interopRequireDefault(require("passport-http-bearer"));

var _jwtSimple = _interopRequireDefault(require("jwt-simple"));

var _user = _interopRequireDefault(require("../dataProvider/user"));

var eventsController = new _eventsController.EventsController();
var router = (0, _express.Router)();
var BearerStrategy = _passportHttpBearer["default"].Strategy;
var SECRET = 'myscecret';

_passport["default"].use(new BearerStrategy(function (token, done) {
  try {
    var _jwt$decode = _jwtSimple["default"].decode(token, SECRET),
        username = _jwt$decode.username,
        password = _jwt$decode.password;

    _user["default"].get(username).then(function (user) {
      if (user.username === username && user.password == password) {
        done(null, username);
        return;
      }

      done(null, false);
    })["catch"](function (error) {
      done(null, false);
    });
  } catch (error) {
    done(null, false);
  }
}));

router.get('/', _passport["default"].authenticate('bearer', {
  session: false
}), eventsController.getAllEvents);
router.post('/', _passport["default"].authenticate('bearer', {
  session: false
}), eventsController.addEvent);
router.get('/:id', _passport["default"].authenticate('bearer', {
  session: false
}), eventsController.getEventInfo);
router.post('/:id', _passport["default"].authenticate('bearer', {
  session: false
}), eventsController.addParticipant);
var _default = router;
exports["default"] = _default;