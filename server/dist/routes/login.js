"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _loginController = require("../controllers/loginController");

var _passport = _interopRequireDefault(require("passport"));

var _passportLocal = _interopRequireDefault(require("passport-local"));

var _jwtSimple = _interopRequireDefault(require("jwt-simple"));

var _user = _interopRequireDefault(require("../dataProvider/user"));

var LocalStrategy = _passportLocal["default"].Strategy;
var SECRET = 'myscecret';
var loginController = new _loginController.LoginController();
var router = (0, _express.Router)();

_passport["default"].use(new LocalStrategy(function (username, password, done) {
  _user["default"].get(username).then(function (user) {
    if (username === user.username && password === user.password) {
      done(null, _jwtSimple["default"].encode({
        username: username,
        password: password
      }, SECRET));
      return;
    }

    done(null, false);
  });
}));

router.get('/', _passport["default"].authenticate('local', {
  session: false
}), loginController.doLogin);
router.post('/', loginController.register);
var _default = router;
exports["default"] = _default;