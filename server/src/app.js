import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import cors from 'cors';

import eventsRouter from './routes/events';
import loginRouter from './routes/login';

import swaggerUi from 'swagger-ui-express'
import swaggerDocument from  '../swaggerDoc.json'

const app = express();

 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT ,DELETE');
  next();
});

app.use('/events', eventsRouter);
app.use('/login', loginRouter);

var config = { db: 'mongodb://localhost/events_app' };
mongoose.connect(
    config.db, 
    { useNewUrlParser: true, useUnifiedTopology: true });

export default app;