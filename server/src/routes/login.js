import { Router } from 'express';
import {LoginController} from '../controllers/loginController';
import passport from 'passport';
import localStrategy from 'passport-local';
import jwt from 'jwt-simple';

import userProvider from '../dataProvider/user';

const LocalStrategy = localStrategy.Strategy;
const SECRET = 'myscecret';

const loginController = new LoginController();
const router = Router();

passport.use(
    new LocalStrategy((username, password, done) => {
        userProvider.get(username)
        .then((user)=> {
            if (username === user.username && password === user.password) {
                done(null, jwt.encode({username, password}, SECRET));
                return;
            }
            done(null, false);
        });
    })
);

router.get('/', passport.authenticate('local', { session: false }), loginController.doLogin);
router.post('/', loginController.register);

export default router;