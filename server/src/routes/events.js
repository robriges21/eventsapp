import { Router } from 'express';
import { EventsController } from '../controllers/eventsController';
import passport from 'passport';
import bearerStrategy from 'passport-http-bearer';
import jwt from 'jwt-simple';

import userProvider from '../dataProvider/user';

const eventsController = new EventsController();
const router = Router();

const BearerStrategy = bearerStrategy.Strategy;
const SECRET = 'myscecret';

passport.use(new BearerStrategy((token, done) => {
    try {
        const { username, password } = jwt.decode(token, SECRET);
        userProvider.get(username)
        .then((user) => {
            if(user.username === username && user.password == password){
                done(null, username);
                return;
            }
            done(null, false);
        })
        .catch(error => {done(null, false);});        
    } catch (error) {
        done(null, false);
    }
}));

router.get('/', passport.authenticate('bearer', { session: false }), eventsController.getAllEvents);
router.post('/', passport.authenticate('bearer', { session: false }), eventsController.addEvent);
router.get('/:id', passport.authenticate('bearer', { session: false }), eventsController.getEventInfo);
router.post('/:id', passport.authenticate('bearer', { session: false }), eventsController.addParticipant);

export default router;