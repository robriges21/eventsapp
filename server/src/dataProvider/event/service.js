function eventService(eventModel) {
    const service = {
        create,
        findAll,
        findOne,
        remove,
        findOneAndUpdate,
    };

    function create(values) {
        const event = eventModel(values);
        return save(event);
    }

    function findAll() {
        return new Promise(function (resolve, reject) {
            eventModel.find({}, function (err, events) {
                if (err) reject(err);
                resolve(events);
            });
        });
    }

    function findOne(eventId) {
        return new Promise(function (resolve, reject) {
            eventModel.findOne({ "_id": eventId }, function (err, events) {
                if (err) reject(err);
                resolve(events);
            });
        });
    }

    function findOneAndUpdate(obj = {}) {
        return new Promise(function (resolve, reject) {
            eventModel.findOne({ "_id": obj.id }, function (err, event) {
                if (err) reject(err);
                event.subscribed.push(obj.participant);
                event.save()
                    .then(() => resolve(event))
                    .catch(err => resolve(err));
            });
        });
    }

    function remove(options) {
        return new Promise(function (resolve, reject) {
            eventModel.findOneAndRemove(options, function (err) {
                if (err) reject(err);
                resolve();
            });
        });
    }

    function save(event) {
        return new Promise(function (resolve, reject) {
            event.save(function (err) {
                if (err) reject(err);
                resolve('Event created!');
            });
        });
    }

    return service;
}

export default eventService;