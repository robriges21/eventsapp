import { Schema, model } from 'mongoose';

var eventSchema = new Schema({
    capacity: { 
        type: Number,
        require: [true, 'Capacity field is required']
    },
    description: {
        type: String,
        required: [true, 'Description field is required']
    },
    date: {
        type: Date,
        default: Date.now,
        required: [true, 'Date field is required']
    },
    location: {
        type: String,
        required: [true, 'Location field is required']
    },
    subscribed: [{
        id: { type: String, required: [true, 'Date Subscribed field is required']},
        date: { type: Date, required: [true, 'Date Subscribed field is required']},
        price: { type: Number, required: [true, 'Price field is required']},
        status: {
            type: String,
            enum : ['Pendent', 'Confirmed', 'Canceled'],
            default: 'Pendent'
        }
      }]
    }, { collection: 'event1' }
);

export default model('Event', eventSchema);