import event from './event';
import eventService from './service';

const dataProvider = eventService(event);

class EventProvider{
    constructor(dataProvider){
        this.provider =  dataProvider;
    }

    async add(event){
        this.provider.create(event)
            .then(() => true)
            .catch(() => false);
    }

    async getAll(){
        return this.provider.findAll()
            .then((result) => result)
            .catch(() => null);;
    }

    async getById(id){
        return this.provider.findOne(id)
            .then((result) => result)
            .catch(() => null);;
    }

    async addParticipant(id, participant){
        return this.provider.findOneAndUpdate({id, participant})
            .then((result) => result)
            .catch(() => null);;
    }
}

export default new EventProvider(dataProvider);