import { Schema as _Schema, model } from 'mongoose';
var Schema = _Schema;

var userSchema = new Schema({
    name: { 
        type: String,
        require: [true, 'Name field is required']
    },
    username: {
        type: String,
        required: [true, 'User Name field is required']
    },
    email: {
        type: String,
        required: [true, 'Date field is required']
    },
    password: {
        type: String,
        required: [true, 'Password field is required']
    }

    }, { collection: 'user2' }
);
export default model('User', userSchema);