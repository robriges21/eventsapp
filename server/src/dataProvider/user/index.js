import user from './user';
import userService from './service';

const dataProvider = userService(user);

class UserProvider{
    constructor(dataProvider){
        this.provider =  dataProvider;
    }

    async add(user){
        this.provider.create(user)
            .then((res) => res)
            .catch((err) => err);
    }

    async get(email){
        return this.provider.findOne(email)
            .then((result) => result)
            .catch((err) => err);;
    }
}

export default new UserProvider(dataProvider);