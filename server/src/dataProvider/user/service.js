function userService(userModel) {
    const service = {
        create,
        findOne,
        findOneAndUpdate,
    };

    function create(values) {
        const user = userModel(values);
        return save(user);
    }

    function findOne(username) {
        return new Promise(function (resolve, reject) {
            userModel.findOne({ "username": username }, function (err, user) {
                if (err) reject(err);
                resolve(user);
            });
        });
    }

    function findOneAndUpdate(obj = {}) {
        return new Promise(function (resolve, reject) {
            userModel.findOne({ "email": obj.email }, function (err, user) {
                if (err) reject(err);
                user.save()
                    .then(() => resolve(user))
                    .catch(err => resolve(err));
            });
        });
    }

    function save(user) {
        return new Promise(function (resolve, reject) {
            user.save(function (err) 
            {
                if (err) reject(err);
                resolve('User created!');
            });
        });
    }

    return service;
}

export default userService;