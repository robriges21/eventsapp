import eventProvider from '../dataProvider/event';

export class EventsController {
    async getAllEvents(request, response){
        const result = await eventProvider.getAll();

        result 
            ? response.jsonp(result)
            : response.sendStatus(500);
    }

    async addEvent(request, response){
        const event = request.body;
        const result = await eventProvider.add(event);

        result
         ? response.sendStatus(500)
         : response.sendStatus(200);
    }

    async getEventInfo(request, response){
        const id = request.params.id;
        
        const result = await eventProvider.getById(id);

        result 
            ? response.jsonp(result)
            : response.sendStatus(500);
    }

    async addParticipant(request, response){
        const id = request.params.id;
        const participant = request.body;

        const result = await eventProvider.addParticipant(id, participant);

        result 
            ? response.jsonp(result)
            : response.sendStatus(500);
    }

    async removeParticipant(request, response){

    }
}