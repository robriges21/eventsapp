import userProvider from '../dataProvider/user';

export class LoginController {
    async doLogin(request, response){
        response.send({
            token: request.user,
        });
    }

    async register(request, response){
        const user = request.body;
        const result = await userProvider.add(user);
        console.log(result);
        result
         ? response.sendStatus(500)
         : response.sendStatus(200);
    }
}